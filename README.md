Compiled Earth System Science Point Datasets
================
Created and maintained by: OpenGeoHub foundation
(<a href="mailto:tom.hengl@OpenGeoHub.org" class="email">tom.hengl@OpenGeoHub.org</a>)
Last compiled on: 16 February, 2021



-   [![alt text](tex/R_logo.svg.png "About") About](#alt-text-about)
    -   [Overview](#overview)
    -   [Data license](#data-license)
    -   [Other similar repositories /
        projects](#other-similar-repositories-projects)
    -   [Machine Learning-ready data](#machine-learning-ready-data)
-   [![alt text](tex/R_logo.svg.png "Data sets") Data
    sets](#alt-text-data-sets)
    -   [Relief / geology](#relief-geology)
    -   [Land cover, land use and administrative
        data](#land-cover-land-use-and-administrative-data)
    -   [Vegetation indices](#vegetation-indices)
    -   [Land degradation indices](#land-degradation-indices)
    -   [Climatic and meteo data](#climatic-and-meteo-data)
    -   [Soil properties and classes](#soil-properties-and-classes)
    -   [Potential natural vegetation](#potential-natural-vegetation)
    -   [Hydrology and water dynamics](#hydrology-and-water-dynamics)
-   [References](#references)

[<img src="tex/opengeohub_logo_ml.png" alt="OpenGeoHub logo" width="350"/>](https://opengeohub.org)

[<img alt="Creative Commons License" style="border-width:0" src="https://i.creativecommons.org/l/by-sa/4.0/88x31.png" />](http://creativecommons.org/licenses/by-sa/4.0/)

This work is licensed under a [Creative Commons Attribution-ShareAlike
4.0 International
License](http://creativecommons.org/licenses/by-sa/4.0/).

![alt text](tex/R_logo.svg.png "About") About
---------------------------------------------

#### Overview

**Global Earth System Science Point Datasets** repository contains
compiled and analysis ready point (spatial) data sets that can be used
for training global spatial prediction models or similar analysis. It is
has been developed within the www.OpenLandMap.org project and its main
objective is [**to support a more reproducible and a more collaborative
global modeling and analysis of environmental and Earth System Science
variables**](https://opengeohub.org/contribute-training-data-landgis).
We focus on meteorological variables (Sparks, Hengl, & Nelson,
[2017](#ref-sparks2017gsodr)), land cover and vegetation observations
(Fritz et al., [2017](#ref-Fritz2017SD); Hengl et al.,
[2018](#ref-hengl2018global)), plant roots (Iversen et al.,
[2018](#ref-FRED2018)) and plant traits (Kattge et al.,
[2020](#ref-kattge2020try)), soil variables (Batjes et al.,
[2017](#ref-batjes2017wosis); Hengl et al.,
[2017](#ref-hengl2017soilgrids250m); Panagos et al.,
[2017](#ref-panagos2017global)), water dynamics (Pena Luque,
[2018](#ref-pena2018synergies)), and measurements of fluxes and
ecosystem services (Jian et al., [2020](#ref-essd-2020-136); Kulmala,
[2018](#ref-kulmala2018build); Ukkola, Haughton, De Kauwe, Abramowitz, &
Pitman, [2017](#ref-ukkola2017fluxnetlsm)), although the list will be
continuously updated and extended.

We bind and *clean* the data using R packages including
[plyr](http://had.co.nz/plyr), [dplyr](https://dplyr.tidyverse.org/) and
other tidyverse.org packages. We closely follow the specifications of
various data standards including the the
<a href="https://www.r-spatial.org/" class="uri">https://www.r-spatial.org/</a>
protocols and standards (Bivand, Pebesma, & Gomez-Rubio,
[2013](#ref-asdar); Wickham & Grolemund, [2017](#ref-r4ds)), then export
data into R and [OpenML.org foundation](https://docs.openml.org/)
(Casalicchio et al., [2017](#ref-casalicchio2017openml)) compatible
formats to make the data more usable for modeling purposes. The
visualizations below are based on using the [Interrupted Goode
homolosine](https://wilkelab.org/practicalgg/articles/goode.html)
projection (an equal area projection for the global land mask).

#### Data license

If not specified otherwise, www.OpenLandMap.org produces open data,
licensed under the [Open Data Commons Open Database License
(ODbL)](https://opendatacommons.org/licenses/odbl/) and/or [Creative
Commons Attribution-ShareAlike 4.0 International license (CC
BY-SA)](https://creativecommons.org/licenses/by-sa/4.0/legalcode).

Users are advised to contact the original data providers for original
copies of point / tabular data indicated in the text above each imported
dataset. If your data set is not cited correctly or you think is not
imported and used correctly, please contact us (e-mail:
<a href="mailto:support@opengeohub.org" class="email">support@opengeohub.org</a>)
and we will do our best to adjust and correct. This site and many other
related services are formally operated by the OpenGeoHub Foundation on
behalf of the community. Use of all OpenGeoHub Foundation operated
services is subject to our [Terms &
Conditions](https://opengeohub.org/general-terms-and-conditions) and our
[Privacy Policy](https://opengeohub.org/privacy-policy/).

Learn more about this repository and how to contribute:

-   [OpenLandMap.org project
    backgrounds](https://opengeohub.org/about-landgis),
-   How to download and load the data,
-   [How to submit a new data
    set](https://opengeohub.org/contribute-training-data-openlandmap),
-   [Report an issue or
    bug](https://gitlab.com/openlandmap/compiled-ess-point-data-sets/-/issues),

#### Other similar repositories / projects

Other global station data / monitoring projects of interest:

-   [Degree Confluence project](http://confluence.org/),
-   [Fine Root Ecology Database (FRED)](https://roots.ornl.gov/),
-   [FLUXNET global network](https://fluxnet.fluxdata.org/),
-   [Global Biodiversity Information Facility (GBIF)
    data](https://www.gbif.org),
-   [Global database of soil
    nematodes](https://www.nature.com/articles/s41597-020-0437-3),
-   [Global Forest Biodiversity
    Initiative](https://www.gfbinitiative.org/data),
-   [Global Freshwater Quality Database GEMStat](https://gemstat.org/),
-   [Global Rainfall Erosivity
    Database](https://esdac.jrc.ec.europa.eu/themes/global-rainfall-erosivity),
-   [Global soil macrofauna
    database](http://macrofauna.earthworms.info/),
-   [Global soil respiration database
    (SRDB)](https://github.com/bpbond/srdb),
-   [Geo-wiki data sets](https://www.geo-wiki.org/pages/data),
-   [HydroWeb water levels of rivers and
    lakes](https://www.theia-land.fr/en/product/water-levels-of-rivers-and-lakes-hydroweb/),
-   [International Soil Moisture
    Network](https://ismn.geo.tuwien.ac.at/en/),
-   [International Soil Carbon Network](http://iscn.fluxdata.org/),
-   [LandPKS project](http://portal.landpotential.org/#/landpksmap),
-   [Long Term Ecological Research (LTER) Network
    sites](https://lternet.edu/site/),
-   [NASA Global Landslide Catalog
    (GLC)](https://data.nasa.gov/Earth-Science/Global-Landslide-Catalog/h9d8-neg4),
-   [NOAA Global Surface Summary of the Day -
    GSOD](https://catalog.data.gov/dataset/global-surface-summary-of-the-day-gsod),
-   [TRY plant trait database](https://www.try-db.org/),
-   [USGS Global Earthquake Hazards
    Database](https://earthquake.usgs.gov/data/),
-   [WorldBank Catalog](https://datacatalog.worldbank.org/),
-   [WMO World Weather
    Watch](http://public.wmo.int/en/programmes/world-weather-watch),
-   [WMO Global Observatory
    System](http://public.wmo.int/en/programmes/global-observing-system),
-   [WoSIS Soil Profile Database](https://www.isric.org/explore/wosis),
-   [WRI Global Forest Watch
    Fires](https://fires.globalforestwatch.org/),

#### Machine Learning-ready data

Tables **classification-/** and/or **regression-matrix** contain values
of target variables plus all global covariate layers (available via
www.OpenLandMap.org) i.e. these are tables that can be directly used for
testing various machine learning algorithms. Test your own approaches
and algorithms and let us know if you discover some significant
relationship or pattern (read more in: [*“OpenLandMap: enabling Machine
Learning for Global
Good”*](https://opengeohub.org/openlandmap-using-machine-learning-global-good)).
Currently [available global
layers](https://zenodo.org/search?page=1&size=20&q=LandGIS) include
(Hengl & MacMillan, [2019](#ref-hengl2019predictive)):

-   DEM-derived surfaces — slope, profile curvature, Multiresolution
    Index of Valley Bottom Flatness (VBF), deviation from Mean Value,
    valley depth, negative and positive Topographic Openness and SAGA
    Wetness Index — all based on MERIT DEM (Yamazaki et al.,
    [2019](#ref-yamazaki2019merit)). All DEM derivatives were computed
    using SAGA GIS (Conrad et al., [2015](#ref-gmd-8-1991-2015)),

-   Long-term averaged monthly mean and standard deviation of the
    [FAPAR](https://land.copernicus.eu/global/products/fapar) based on
    the PROBA-V images,

-   Long-term averaged mean monthly surface reflectances for MODIS bands
    4 (NIR) and 7 (MIR). Derived using a stack of MCD43A4 images (Mira
    et al., [2015](#ref-mira2015modis)),

-   Long-term (2000-2018) averaged monthly mean and standard deviation
    of the MODIS land surface temperature (daytime and nighttime).
    Derived using a stack of MOD11A2 LST images (Wan,
    [2006](#ref-wan2006modis)),

-   Land cover classes (2016) based on the ESA’s land cover CCI project
    (Bontemps et al., [2013](#ref-bontemps2013consistent)),

-   Monthly precipitation images based on a weighted average between the
    SM2RAIN (Brocca et al., [2019](#ref-brocca2019sm2rain)), WorldClim
    monthly precipitation (Fick & Hijmans,
    [2017](#ref-fick2017worldclim)) and CHELSA climate (Karger et al.,
    [2017](#ref-karger2017climatologies)),

-   ESA’s long-term averaged mean monthly hours under [snow
    cover](http://maps.elie.ucl.ac.be/CCI/viewer/index.php). Derived
    using MOD10A2 8-day snow occurrence images,

-   Lithologic units (acid plutonics, acid volcanic, basic plutonics,
    basic volcanics, carbonate sedimentary rocks, evaporite, ice and
    glaciers, intermediate plutonics, intermediate volcanics,
    metamorphics, mixed sedimentary rocks, pyroclastics, siliciclastic
    sedimentary rocks, unconsolidated sediment) based on a Global
    Lithological Map GLiM (Hartmann & Moosdorf,
    [2012](#ref-GGGE:GGGE2352)),

-   Landform classes (breaks/foothills, flat plains, high mountains/deep
    canyons, hills, low hills, low mountains, smooth plains) based on
    the USGS’s Map of Global Ecological Land Units (Sayre et al.,
    [2014](#ref-sayre2014new)),

-   Global Water Table Depth in meters (Fan, Li, & Miguez-Macho,
    [2013](#ref-fan2013global)),

-   Landsat-based estimated distribution of Mangroves (Giri et al.,
    [2011](#ref-giri2011status)),

-   Average soil and sedimentary-deposit thickness in meters (Pelletier
    et al., [2016](#ref-Pelletier2016)).

**Disclaimer**: The data is provided “as is”. [OpenGeoHub
foundation](https://opengeohub.org/about) and its suppliers and
licensors hereby disclaim all warranties of any kind, express or
implied, including, without limitation, the warranties of
merchantability, fitness for a particular purpose and non-infringement.
Neither OpenGeoHub foundation nor its suppliers and licensors, makes any
warranty that the Website will be error free or that access thereto will
be continuous or uninterrupted. You understand that you download from,
or otherwise obtain content or services through, the Website at your own
discretion and risk.

![alt text](tex/R_logo.svg.png "Data sets") Data sets
-----------------------------------------------------

#### Relief / geology

-   Under construction,

#### Land cover, land use and administrative data

###### Import steps: [nat.landcover](themes/lcv/NativeLandCover/)

<div class="figure">

<img src="img/lcv_nat.landcover.pnts_sites.png" alt="Native vegetation land cover field observations global compilation." width="100%" />
<p class="caption">
Native vegetation land cover field observations global compilation.
</p>

</div>

Fig. 1: Native vegetation land cover field observations global
compilation.

-   :computer: output RDS files:
    [`lcv_nat.landcover.pnts_sites.rds`](out/rds/lcv_nat.landcover.pnts_sites.rds),
-   :open\_file\_folder: data download:
    [10.5281/zenodo.3631253](https://doi.org/10.5281/zenodo.3631253),
-   :chart\_with\_upwards\_trend: classification-matrix:
    [`lcv_nat.landcover.pnts_sites_cm.rds`](out/rds/lcv_nat.landcover.pnts_sites_cm.rds),
-   :book: citation: Hengl, T., Jung, M., & Visconti, P., (2020).
    **Potential distribution of land cover classes (Potential Natural
    Vegetation) at 250 m spatial resolution (Version v0.1)** \[Data
    set\]. Zenodo.
    <a href="http://doi.org/10.5281/zenodo.3631253" class="uri">http://doi.org/10.5281/zenodo.3631253</a>,  
-   :package: output GPKG files:
    [`lcv_nat.landcover.pnts_sites.gpkg`](out/gpkg/lcv_nat.landcover.pnts_sites.gpkg),
-   :bookmark\_tabs: connected packages / projects: [Geo-wiki
    data](https://www.geo-wiki.org/pages/data), [Nature map
    Explorer](https://explorer.naturemap.earth/),

#### Vegetation indices

-   Under construction,

#### Land degradation indices

-   Under construction,

#### Climatic and meteo data

###### Import steps: gsod.daily

<div class="figure">

<img src="img/clm_gsod.pnts_sites.png" alt="Meteorological stations with daily measurements global compilation." width="100%" />
<p class="caption">
Meteorological stations with daily measurements global compilation.
</p>

</div>

Fig. 2: Meteorological stations with daily measurements global
compilation.

-   :computer: output RDS files: *pending*,
-   :open\_file\_folder: data download:
    [gov.noaa.ncdc:C00516](https://data.noaa.gov/dataset/dataset/global-surface-summary-of-the-day-gsod),
-   :book: citation: Sparks, A.H., Hengl, T., and Nelson, A. (2017).
    **GSODR: Global Summary Daily Weather Data in R**. The Journal of
    Open Source Software, 2(10).
    <a href="https://dx.doi.org/10.21105/joss.00177" class="uri">https://dx.doi.org/10.21105/joss.00177</a>
-   :package: output GPKG files: *pending*,
-   :bookmark\_tabs: connected packages / projects:
    [GSODR](https://github.com/ropensci/GSODR),
    [meteo](https://cran.r-project.org/package=meteo),

#### Soil properties and classes

###### Import steps: [soilchem](themes/sol/SoilChemDB/)

<div class="figure">

<img src="img/sol_chem.pnts_sites.png" alt="Soil profiles and soil samples with chemical and physical properties global compilation." width="100%" />
<p class="caption">
Soil profiles and soil samples with chemical and physical properties
global compilation.
</p>

</div>

Fig. 3: Soil profiles and soil samples with chemical and physical
properties global compilation.

-   :computer: output RDS files:
    [`sol_chem.pnts_horizons.rds`](out/rds/sol_chem.pnts_horizons.rds),
-   :chart\_with\_upwards\_trend: regression-matrix:
    [`sol_chem.pnts_horizons_rm.rds`](out/rds/sol_chem.pnts_horizons_rm.rds),
-   :open\_file\_folder: data download: NA,
-   :book: citation: NA
-   :package: output GPKG files:
    [`sol_chem.pnts_horizons.gpkg`](out/gpkg/sol_chem.pnts_horizons.gpkg),
-   :bookmark\_tabs: connected packages / projects:
    [SOCDR](https://github.com/ISCN/SOCDRaHR2),
    [febr](https://github.com/febr-team/febr-package),
    [soilDB](https://cran.r-project.org/package=soilDB),
    [SoilHealthDB](https://doi.org/10.1038/s41597-020-0356-3),

###### Import steps: [soilhydro](themes/sol/SoilHydroDB/)

<div class="figure">

<img src="img/sol_hydro.pnts_sites.png" alt="Soil profiles and soil samples with physical and hydraulic soil properties properties global compilation." width="100%" />
<p class="caption">
Soil profiles and soil samples with physical and hydraulic soil
properties properties global compilation.
</p>

</div>

Fig. 4: Soil profiles and soil samples with physical and hydraulic soil
properties properties global compilation.

-   :computer: output RDS files:
    [`sol_hydro.pnts_horizons.rds`](out/rds/sol_hydro.pnts_horizons.rds),
    [`sol_ksat.pnts_horizons.rds`](out/rds/sol_ksat.pnts_horizons.rds),
-   :chart\_with\_upwards\_trend: regression-matrix:
    [`sol_hydro.pnts_horizons_rm.rds`](out/rds/sol_hydro.pnts_horizons_rm.rds),
-   :open\_file\_folder: data download:
    [10.5281/zenodo.3752721](https://doi.org/10.5281/zenodo.3752721),
-   :book: citation: Gupta, S., Hengl, T., Lehmann, P., Bonetti, S., and
    Or, D. **SoilKsatDB: global soil saturated hydraulic conductivity
    measurements for geoscience applications**. Earth Syst. Sci. Data
    Discuss.,
    <a href="https://doi.org/10.5194/essd-2020-149" class="uri">https://doi.org/10.5194/essd-2020-149</a>,
    in review, 2020.
-   :package: output GPKG files:
    [`sol_hydro.pnts_horizons.gpkg`](out/gpkg/sol_hydro.pnts_horizons.gpkg),
-   :bookmark\_tabs: connected packages / projects:
    [HydroMe](https://cran.r-project.org/package=HydroMe),
    [soilphysics](https://cran.r-project.org/package=soilphysics),
    [soiltexture](https://cran.r-project.org/package=soiltexture),

#### Potential natural vegetation

###### Import steps: [Biomes](themes/pnv/Biomes/)

<div class="figure">

<img src="img/pnv_biomes.pnts_sites.png" alt="Observations of historic biomes (BIOME 6000 data set) global compilation." width="100%" />
<p class="caption">
Observations of historic biomes (BIOME 6000 data set) global
compilation.
</p>

</div>

Fig. 5: Observations of historic biomes (BIOME 6000 data set) global
compilation.

-   :computer: output RDS files:
    [`pnv_biomes.pnts_sites.rds`](out/rds/pnv_biomes.pnts_sites.rds),
-   :chart\_with\_upwards\_trend: classification-matrix:
    [`pnv_biomes.pnts_sites_cm.rds`](out/rds/pnv_biomes.pnts_sites_cm.rds),
-   :open\_file\_folder: data download:
    [10.7910/DVN/QQHCIK](http://dx.doi.org/10.7910/DVN/QQHCIK),
-   :book: citation: Hengl T, Walsh MG, Sanderman J, Wheeler I, Harrison
    SP, Prentice IC. (2018). **Global mapping of potential natural
    vegetation: an assessment of machine learning algorithms for
    estimating land potential**. PeerJ 6:e5457.
    <a href="https://doi.org/10.7717/peerj.5457" class="uri">https://doi.org/10.7717/peerj.5457</a>
-   :package: output GPKG files:
    [`pnv_biomes.pnts_sites.gpkg`](out/gpkg/pnv_biomes.pnts_sites.gpkg),
-   :bookmark\_tabs: connected packages / projects: [Biome 6000
    project](http://www.bridge.bris.ac.uk/projects/BIOME_6000),

#### Hydrology and water dynamics

-   Under construction,

References
----------

<div id="refs" class="references hanging-indent">

<div id="ref-batjes2017wosis">

Batjes, N. H., Ribeiro, E., Oostrum, A. van, Leenaars, J., Hengl, T., &
Jesus, J. M. de. (2017). WoSIS: Providing standardised soil profile data
for the world. *Earth System Science Data*, *9*(1), 1.
doi:[10.5194/essd-9-1-2017](https://doi.org/10.5194/essd-9-1-2017)

</div>

<div id="ref-asdar">

Bivand, R. S., Pebesma, E., & Gomez-Rubio, V. (2013). *Applied spatial
data analysis with R, second edition*. Springer, NY. Retrieved from
<http://www.asdar-book.org/>

</div>

<div id="ref-bontemps2013consistent">

Bontemps, S., Defourny, P., Radoux, J., Van Bogaert, E., Lamarche, C.,
Achard, F., … others. (2013). Consistent global land cover maps for
climate modelling communities: Current achievements of the esa’s land
cover cci. In *Proceedings of the esa living planet symposium,
edimburgh* (pp. 9–13).

</div>

<div id="ref-brocca2019sm2rain">

Brocca, L., Filippucci, P., Hahn, S., Ciabatta, L., Massari, C., Camici,
S., … Wagner, W. (2019). SM2RAIN–ascat (2007–2018): Global daily
satellite rainfall data from ascat soil moisture observations. *Earth
System Science Data*, *11*(4).

</div>

<div id="ref-casalicchio2017openml">

Casalicchio, G., Bossek, J., Lang, M., Kirchhoff, D., Kerschke, P.,
Hofner, B., … Bischl, B. (2017). OpenML: An R package to connect to the
machine learning platform OpenML. *Computational Statistics*, 1–15.
doi:[10.1007/s00180-017-0742-2](https://doi.org/10.1007/s00180-017-0742-2)

</div>

<div id="ref-gmd-8-1991-2015">

Conrad, O., Bechtel, B., Bock, M., Dietrich, H., Fischer, E., Gerlitz,
L., … Böhner, J. (2015). System for automated geoscientific analyses
(saga) v. 2.1.4. *Geoscientific Model Development*, *8*(7), 1991–2007.
doi:[10.5194/gmd-8-1991-2015](https://doi.org/10.5194/gmd-8-1991-2015)

</div>

<div id="ref-fan2013global">

Fan, Y., Li, H., & Miguez-Macho, G. (2013). Global patterns of
groundwater table depth. *Science*, *339*(6122), 940–943.

</div>

<div id="ref-fick2017worldclim">

Fick, S. E., & Hijmans, R. J. (2017). WorldClim 2: New 1-km spatial
resolution climate surfaces for global land areas. *International
Journal of Climatology*, *37*(12), 4302–4315.

</div>

<div id="ref-Fritz2017SD">

Fritz, S., See, L., Perger, C., McCallum, I., Schill, C., Schepaschenko,
D., … Obersteiner, M. (2017). A global dataset of crowdsourced land
cover and land use reference data. *Scientific Data*, *4*, 170075.
doi:[10.1038/sdata.2017.75](https://doi.org/10.1038/sdata.2017.75)

</div>

<div id="ref-giri2011status">

Giri, C., Ochieng, E., Tieszen, L. L., Zhu, Z., Singh, A., Loveland, T.,
… Duke, N. (2011). Status and distribution of mangrove forests of the
world using earth observation satellite data. *Global Ecology and
Biogeography*, *20*(1), 154–159.

</div>

<div id="ref-GGGE:GGGE2352">

Hartmann, J., & Moosdorf, N. (2012). The new global lithological map
database GLiM: A representation of rock properties at the Earth surface.
*Geochemistry, Geophysics, Geosystems*, *13*(12), n/a–n/a.
doi:[10.1029/2012GC004370](https://doi.org/10.1029/2012GC004370)

</div>

<div id="ref-hengl2017soilgrids250m">

Hengl, T., Jesus, J. M. de, Heuvelink, G. B., Gonzalez, M. R.,
Kilibarda, M., Blagotić, A., … others. (2017). SoilGrids250m: Global
gridded soil information based on machine learning. *PLoS One*, *12*(2).
doi:[10.1371/journal.pone.0169748](https://doi.org/10.1371/journal.pone.0169748)

</div>

<div id="ref-hengl2019predictive">

Hengl, T., & MacMillan, R. A. (2019). *Predictive soil mapping with r*
(p. 370). Wageningen, the Netherlands: Lulu. com.

</div>

<div id="ref-hengl2018global">

Hengl, T., Walsh, M. G., Sanderman, J., Wheeler, I., Harrison, S. P., &
Prentice, I. C. (2018). Global mapping of potential natural vegetation:
An assessment of machine learning algorithms for estimating land
potential. *PeerJ*, *6*, e5457.
doi:[10.7717/peerj.5457](https://doi.org/10.7717/peerj.5457)

</div>

<div id="ref-FRED2018">

Iversen, C., Powell, A., McCormack, M., Blackwood, C., Freschet, G.,
Kattge, J., … C, V. (2018). *Fine-Root Ecology Database (FRED): A Global
Collection of Root Trait Data with Coincident Site, Vegetation, Edaphic,
and Climatic Data, Version 2*. TES SFA, U.S. Department of Energy, Oak
Ridge, Tennessee, U.S.A.: Oak Ridge National Laboratory. Retrieved from
<https://doi.org/10.25581/ornlsfa.012/1417481>

</div>

<div id="ref-essd-2020-136">

Jian, J., Vargas, R., Anderson-Teixeira, K., Stell, E., Herrmann, V.,
Horn, M., … Bond-Lamberty, B. (2020). A restructured and updated global
soil respiration database (srdb-v5). *Earth System Science Data
Discussions*, *2020*, 1–19.
doi:[10.5194/essd-2020-136](https://doi.org/10.5194/essd-2020-136)

</div>

<div id="ref-karger2017climatologies">

Karger, D. N., Conrad, O., Böhner, J., Kawohl, T., Kreft, H.,
Soria-Auza, R. W., … Kessler, M. (2017). Climatologies at high
resolution for the earth’s land surface areas. *Scientific Data*, *4*,
170122.

</div>

<div id="ref-kattge2020try">

Kattge, J., Bönisch, G., Dı́az, S., Lavorel, S., Prentice, I. C.,
Leadley, P., … others. (2020). TRY plant trait database–enhanced
coverage and open access. *Global Change Biology*, *26*(1), 119–188.

</div>

<div id="ref-kulmala2018build">

Kulmala, M. (2018). Build a global earth observatory. Nature Publishing
Group.
doi:[10.1038/d41586-017-08967-y](https://doi.org/10.1038/d41586-017-08967-y)

</div>

<div id="ref-mira2015modis">

Mira, M., Weiss, M., Baret, F., Courault, D., Hagolle, O.,
Gallego-Elvira, B., & Olioso, A. (2015). The modis (collection v006)
brdf/albedo product mcd43d: Temporal course evaluated over agricultural
landscape. *Remote Sensing of Environment*, *170*, 216–228.

</div>

<div id="ref-panagos2017global">

Panagos, P., Borrelli, P., Meusburger, K., Yu, B., Klik, A., Lim, K. J.,
… others. (2017). Global rainfall erosivity assessment based on
high-temporal resolution rainfall records. *Scientific Reports*, *7*(1),
1–12.

</div>

<div id="ref-Pelletier2016">

Pelletier, J. D., Broxton, P. D., Hazenberg, P., Zeng, X., Troch, P. A.,
Niu, G.-Y., … Gochis, D. (2016). A gridded global data set of soil,
immobile regolith, and sedimentary deposit thicknesses for regional and
global land surface modeling. *Journal of Advances in Modeling Earth
Systems*, *8*.
doi:[10.1002/2015MS000526](https://doi.org/10.1002/2015MS000526)

</div>

<div id="ref-pena2018synergies">

Pena Luque, S. (2018). Synergies of a high resolution and multi-source
water surface product "surfwater" in an integrated water database:
Hydroweb-ng. *AGUFM*, *2018*, H23C–07.

</div>

<div id="ref-sayre2014new">

Sayre, R., Dangermond, J., Frye, C., Vaughan, R., Aniello, P., Breyer,
S., … others. (2014). *A new map of global ecological land units—an
ecophysiographic stratification approach*. Washington, DC: USGS /
Association of American Geographers.

</div>

<div id="ref-sparks2017gsodr">

Sparks, A., Hengl, T., & Nelson, A. (2017). GSODR: Global Summary Daily
Weather Data in R. *Journal of Open Source Software*, *2*(10), 177.

</div>

<div id="ref-ukkola2017fluxnetlsm">

Ukkola, A., Haughton, N., De Kauwe, M., Abramowitz, G., & Pitman, A.
(2017). FluxnetLSM R package (v1. 0): a community tool for processing
FLUXNET data for use in land surface modelling. *Geosci. Model Dev.*
doi:[10.5194/gmd-10-3379-2017](https://doi.org/10.5194/gmd-10-3379-2017)

</div>

<div id="ref-wan2006modis">

Wan, Z. (2006). *MODIS land surface temperature products users’ guide*
(p. 35). ICESS, University of California.

</div>

<div id="ref-r4ds">

Wickham, H., & Grolemund, G. (2017). *R for data science*. O’Reilly.
Retrieved from <http://r4ds.had.co.nz/>

</div>

<div id="ref-yamazaki2019merit">

Yamazaki, D., Ikeshima, D., Sosa, J., Bates, P. D., Allen, G. H., &
Pavelsky, T. M. (2019). MERIT hydro: A high-resolution global
hydrography map based on latest topography dataset. *Water Resources
Research*, *55*(6), 5053–5073.

</div>

</div>
